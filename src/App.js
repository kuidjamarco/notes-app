import './App.css';
import {useState} from 'react';


function Note(props){
  return(
    <div className="note">
      <div className="menu">
        <i className="fa fa-edit" onClick={()=>props.mhandle(props.id)}></i>
        <i className="fa fa-trash" id={props.id} onClick={props.ftn}></i>
      </div>
      <textarea onChange={(event)=>props.write(event, props.id)} value={props.msg} disabled={props.mode}></textarea>
    </div>
  );
}

function App() {
  let [notes, setNotes] = useState([])
  let [notesId, setNotesId] = useState(notes.length)
  let handleAddNote=()=>{
    setNotes([...notes, {id:notesId, value:"", mode:true}]);
    setNotesId(notesId+1)
  }

  let handleDeleteNote=(event)=>{
    const trashElt = event.target;    
    setNotes(notes.filter(note=>note.id !== Number(trashElt.id)))
  }

  let handleNoteMode=(noteId)=>{
    console.log("mode")
    let newNotes = notes.map(note => {
      if(note.id === Number(noteId)){
        return {...note, mode:!note.mode}
      } else return note
    });
    setNotes(newNotes);
  }

  let writeOnNote = (event, noteId) =>{
    let areaValue = event.target.value;
    let newNotes = notes.map(note => {
      if(note.id === Number(noteId)){
        return {...note, value:areaValue}
      } else return note
    });
    setNotes(newNotes);
  }

  return (
    <div className="App">
      <header className="App-header">
        <button className="adding" onClick={handleAddNote}>+ add note</button>
      </header>
      <div className="container">{
        notes.map(note=><Note key={note.id} write={writeOnNote} id={note.id} msg={note.value} ftn={handleDeleteNote} mode={note.mode} mhandle={handleNoteMode}/>)
      }</div>
    </div>
  );
}

export default App;
